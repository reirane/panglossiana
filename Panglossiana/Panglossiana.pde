/* Copyright (c) 2020 Leilane Cruz e outros.
 * For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */


// Resolvendo exibir imagem em posição relativa às dimensões da tela

PImage imagem;
PImage[] imagens;
int numImagens = 2;
int imagemAtual = 0;
float posCaptchaX = 0.5;
float posCaptchaY = 0.5;
float larguraCaptcha = 0.8;
float larguraCaptchaPx;
float alturaCaptchaPx;

void setup()
{
  size(500, 400);
  imagem = loadImage("img001.jpg"); // Carrega a imagem 
  imagens = new PImage[numImagens];
  
  for(int i = 0; i < numImagens; i++)
  {
    imagens[i] = loadImage("img00" + i + ".jpg"); 
  }
  
  larguraCaptchaPx = larguraCaptcha*width; // Define a largura do captcha em pixel de acordo com largura da tela
  alturaCaptchaPx = larguraCaptchaPx/(imagem.width/imagem.height); // Garante que imagem seja exibida sem distorções
}

void draw()
{
  imageMode(CENTER);
  image(imagens[imagemAtual], posCaptchaX * width, posCaptchaY * height, larguraCaptchaPx, alturaCaptchaPx); // Exibe a imagem na posição 
}

void keyPressed () {
  imagemAtual++;
  if (imagemAtual > numImagens-1) {
    imagemAtual = 0;
  }
}