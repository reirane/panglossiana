# Panglossiana

Panglossiana é um projeto de disco virtual. Entendendo-se álbum/disco como um registro sonoro feito em um dado suporte, Panglossiana propõe explorar as fronteiras do suporte digital para realizar um registro de composição experimental. 

A parte do projeto desenvolvida nesse repositório é uma proposta de composição/audição com o recurso do CAPTCHA. Carregaremos 100 imagens de tipografia com glitch e associaremos a cada uma delas uma micropeça de 1 ou 2 segundos com voz-ruído granulada para tocar quando um ouvinte apertar no ícone de reprodução do som.

Como as letras do teclado não correspondem ao glitch, qualquer coisa que o ouvinte digitar sempre dará errado (a imagem só muda quando a pessoa apertar no ícone 'Alterar CAPTCHA' com as setinhas em círculo).

O barulho associado ao glitch é analogia à frustração da tentativa fadada ao erro na interação com o CAPTCHA. Para o otimista, o panglossiano, o incômodo causado pela confusão da tipografia defeituosa acompanhada da agonia sonora do barulho é o próprio incômodo da pessoa de atitude positiva quando se vê confrontada com o  fracasso inevitável.

Linguagem: Processing 3.0